from src.airplane import Airplane
def test_news_airplane_mileage_and_seats_taken():
    input_airplane = Airplane("Chopin", 200)
    assert input_airplane.mileage == 0
    assert input_airplane.seats_taken == 0

def test_fly_5000_km():
    input_airplane = Airplane("Chopin", 200)
    input_airplane.fly(5000)
    assert input_airplane.mileage == 5000

def test_fly_5000_and_3000_km():
    input_airplane = Airplane("Chopin", 200)
    input_airplane.fly(5000)
    input_airplane.fly(3000)
    assert input_airplane.mileage == 8000

def test_if_9999_km_does_not_need_service():
    input_airplane = Airplane("Chopin", 200)
    input_airplane.fly(9999)
    assert input_airplane.is_service_required() == False

def test_if_10001_km_needs_service():
    input_airplane = Airplane("Chopin", 200)
    input_airplane.fly(10001)
    assert input_airplane.is_service_required() == True

def test_if_200_seats_airplane_after_180_has_20_seats_avaible():
    input_airplane = Airplane("Chopin", 200)
    input_airplane.board_passengers(180)
    assert input_airplane.get_availabe_seats() == 20

def test_if_200_seats_airplane_after_201_has_0_seats_avaible():
    input_airplane = Airplane("Chopin", 200)
    input_airplane.board_passengers(201)
    assert input_airplane.get_availabe_seats() == 0