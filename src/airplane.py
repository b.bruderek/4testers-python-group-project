class Airplane:
    def __init__(self, name, seats_number):
        self.name = name
        self.seats_number = seats_number
        self.mileage = 0
        self.seats_taken = 0

    def fly(self, distance):
#        self.mileage = self.mileage + distance
        self.mileage += distance

    def is_service_required(self):
        return True if self.mileage > 10_000 else False

    def board_passengers(self, number_of_passengers):
        if number_of_passengers <= self.get_availabe_seats():
            self.seats_taken += number_of_passengers
        else:
            self.seats_taken = self.seats_number

    def get_availabe_seats(self):
        return self.seats_number - self.seats_taken


if __name__ == '__main__':
    # stwórz dwa różne samoloty
    airplane_chopin = Airplane("Chopin", 300)
    airplane_kosciuszko = Airplane("Kościuszko", 500)
    # przeleć nimi trochę kilometrów
    airplane_chopin.fly(10_000)
    print("Distance airplane Chopin:", airplane_chopin.mileage)
    airplane_kosciuszko.fly(20_000)
    print("Distance airplane Kościuszko:", airplane_kosciuszko.mileage)
    # Sprawdź czy samolot wymaga serwisu
    service_required_chopin = airplane_chopin.is_service_required()
    print("Chopin - is service required:", service_required_chopin)
    service_required_kosciuszko = airplane_kosciuszko.is_service_required()
    print("Kościuszko - is service required:", service_required_kosciuszko)
    # Załaduj pasażerów na pokład
    airplane_chopin.board_passengers(200)
    print("Chopin - current number of seats occupied:", airplane_chopin.seats_taken)
    airplane_kosciuszko.board_passengers(150)
    print("Kościuszko - current number of seats occupied:", airplane_kosciuszko.seats_taken)
    # Sprawdź ilość dostępnych nadal miejsc
    chopin_avaible_seats = airplane_chopin.get_availabe_seats()
    print("Chopin - avaible seats:", chopin_avaible_seats)
    kosciuszko_avaible_sets = airplane_kosciuszko.get_availabe_seats()
    print("Kościuszko - avaible seats:", kosciuszko_avaible_sets)
